//
//  DetailViewController.swift
//  ARKitBasics
//
//  Created by user on 2018/10/14.
//  Copyright © 2018年 Apple. All rights reserved.
//

import UIKit
import SceneKit

class DetailViewController: UIViewController {
    
    var geometryNode : SCNNode!
    var isRound = false

    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        print("BackButton2 is tapped!")
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let scene = SCNScene()
        
        let scnView = self.view as! SCNView
        scnView.backgroundColor = UIColor.black
        scnView.scene = scene
        scnView.showsStatistics = true
        
        let sphere:SCNGeometry = SCNSphere(radius: 2)
        geometryNode = SCNNode(geometry: sphere)
        scene.rootNode.addChildNode(geometryNode)

        let cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        cameraNode.position = SCNVector3(x: 0, y: 0, z: 10)
        scene.rootNode.addChildNode(cameraNode)
        
        let lightNode = SCNNode()
        lightNode.light = SCNLight()
        lightNode.light!.type = .omni
        lightNode.position = SCNVector3(x: 0, y: 10, z: 10)
        scene.rootNode.addChildNode(lightNode)
        
        sphere.firstMaterial?.diffuse.contents = UIImage(named: "Sample.png")
        geometryNode.runAction(SCNAction.repeatForever(SCNAction.rotateBy(x: 0, y: 2, z: 0, duration: 2)))
        isRound = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        scnView.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleTap(_ gestureRecognize: UIGestureRecognizer) {
        print("タップされました")
        
        if isRound{
            geometryNode.runAction(SCNAction.repeatForever(SCNAction.rotateBy(x: 0, y: 0, z: 0, duration: 0)))
            isRound = false
            print("isRound is true -> false")
        }else{
            geometryNode.runAction(SCNAction.repeatForever(SCNAction.rotateBy(x: 0, y: 2, z: 0, duration: 2)))
            isRound = true
            print("isRound is false -> true")
        }
    }
}
/*
 //　グローバルなオブジェクトとしてボールを設定
 var objectNode : SCNNode!
 var printerNode : SCNNode!
 //var ballNode : SCNNode!
 
 override func viewDidLoad() {
 super.viewDidLoad()
 
 // シーン設定
 let scene = PrinterScene()
 
 // SCNView 設定
 let scnView = self.view as! SCNView
 scnView.backgroundColor = UIColor.black
 scnView.scene = scene
 scnView.showsStatistics = true
 scnView.allowsCameraControl = true
 
 
 let sphere = SCNSphere(radius: 2)
 objectNode = SCNNode(geometry: sphere)
 objectNode.position.y = 2
 // scene.rootNode.addChildNode(objectNode)
 
 guard self.printerNode == nil else { return }
 if let printerNode = SCNScene(named: "Assets.scnassets/printer.dae")?.rootNode.childNode(withName: "mfu_base", recursively: true) {
 for n in printerNode.childNodes.makeIterator() {
 n.scale = SCNVector3(0.02, 0.02, 0.02)
 }
 printerNode.physicsBody = SCNPhysicsBody(type: .static, shape: nil)
 printerNode.addChildNode(printerNode)
 printerNode.position.y = 10
 self.printerNode = printerNode
 }
 
 scnView.scene?.rootNode.addChildNode(printerNode)
 
 /* 以下で、ballNodeの設定を行う */
 // ジオメトリ
 //let ball = SCNSphere(radius: 0.5)
 //ballNode = SCNNode(geometry: ball)
 //ballNode.position.y = 4
 
 /*
 // アニメーション
 ballNode.runAction(SCNAction.sequence([
 SCNAction.wait(duration: 22, withRange:4),
 SCNAction.fadeOut(duration: 1),
 SCNAction.removeFromParentNode()
 ]))
 */
 
 // 追加分
 //let physicsBall = SCNPhysicsShape(node: ballNode, options: nil)
 //ballNode.physicsBody = SCNPhysicsBody(type: .dynamic, shape: physicsBall)
 
 
 let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
 scnView.addGestureRecognizer(tapGesture)
 }
 
 override func didReceiveMemoryWarning() {
 super.didReceiveMemoryWarning()
 }
 
 @objc func handleTap(_ gestureRecognize: UIGestureRecognizer) {
 print("タップされました")
 
 //let scnView = self.view as! SCNView
 //scnView.scene?.rootNode.addChildNode(printerNode)
 //scnView.scene?.rootNode.addChildNode(objectNode)
 
 //scnView.scene?.rootNode.addChildNode(ballNode.clone())
 
 // 画面タップごとに、運動動作を持ったボールが落ちて来る
 /*
 let scnView = self.view as! SCNView
 
 let ball = SCNSphere(radius: 0.5)
 let ballNode = SCNNode(geometry: ball)
 ballNode.position.y = 2
 ballNode.physicsBody = SCNPhysicsBody(type: .dynamic, shape: nil)
 
 let physicsBall = SCNPhysicsShape(node: ballNode, options: nil)
 ballNode.physicsBody = SCNPhysicsBody(type: .dynamic, shape: physicsBall)
 
 scnView.scene?.rootNode.addChildNode(ballNode)
 */
 }
 
 }
 */


