//
//  PrinterScene.swift
//  ARKitBasics
//
//  Created by user on 2018/10/14.
//  Copyright © 2018年 Apple. All rights reserved.
//

import UIKit
import SceneKit

class PrinterScene: SCNScene {
    
    override init() {
        super.init()
        
        self.setUpScene()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpScene(){
        
        /*
        // Sphere
         let sphere:SCNGeometry = SCNSphere(radius: 2)
         let geometryNode = SCNNode(geometry: sphere)
         self.rootNode.addChildNode(geometryNode)
        */
        
        // Turus
        /*
         let turus = SCNTorus(ringRadius: 2.0, pipeRadius: 0.35)
         let turusNode = SCNNode(geometry: turus)
         self.rootNode.addChildNode(turusNode)
         
         turusNode.runAction(
         SCNAction.repeatForever(
         SCNAction.sequence([
         SCNAction.move(to: SCNVector3(x: -0.5, y: 2.0, z: 2.0), duration: 0.5),
         //SCNAction.rotateBy(x: CGFloat(Float.pi * 1.0), y: 0, z: 0, duration: 0.5),
         SCNAction.rotateBy(x: 0, y: 0, z: CGFloat(Float.pi * 1.0), duration: 0.5)])))
         */
        
        // Ball
        /*
        let ball = SCNSphere(radius: 2)
        let ballNode = SCNNode(geometry: ball)
        ballNode.position.y = 2
        ballNode.physicsBody = SCNPhysicsBody(type: .dynamic, shape: nil)
        */
        
        /*
        // Floor
        let floor = SCNFloor()
        let floorNode = SCNNode(geometry: floor)
        floorNode.position = SCNVector3(0, -4, 0)
        floorNode.physicsBody = SCNPhysicsBody(type: .static, shape: nil)
        self.rootNode.addChildNode(floorNode)
        */
        
        
        // omni Light
        let lightNode = SCNNode()
        lightNode.light = SCNLight()
        lightNode.light!.type = .omni
        lightNode.position = SCNVector3(x:0, y: 10, z:10)
        self.rootNode.addChildNode(lightNode)
        
        // ambient Light
        let ambientLightNode = SCNNode()
        ambientLightNode.light = SCNLight()
        ambientLightNode.light!.type = .ambient
        ambientLightNode.light!.color = UIColor.darkGray
        self.rootNode.addChildNode(ambientLightNode)
        
        // カメラ
        let cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        cameraNode.position = SCNVector3(x: 0, y: 0, z: 10)
        self.rootNode.addChildNode(cameraNode)
    }
}
